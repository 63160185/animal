/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.animal;

/**
 *
 * @author ACER
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Huhew");
        h1.eat();
        h1.walk();
        h1.run();
        h1.speak();
        h1.sleep();
        System.out.println(h1);
        Except();

        Cat c1 = new Cat("Mumin");
        c1.eat();
        c1.walk();
        c1.run();
        c1.speak();
        c1.sleep();
        c1.toString();
        System.out.println(c1);
        Except();

        Dog d1 = new Dog("Temmi");
        d1.eat();
        d1.walk();
        d1.run();
        d1.speak();
        d1.sleep();
        d1.toString();
        System.out.println(d1);
        Except();

        Crocodile co1 = new Crocodile("Itu");
        co1.eat();
        co1.crawl();
        co1.walk();
        co1.speak();
        co1.sleep();
        co1.toString();
        System.out.println(co1);
        Except();

        Bat b1 = new Bat("Yuri");
        b1.eat();
        b1.fly();
        b1.walk();
        b1.speak();
        b1.sleep();
        b1.toString();
        System.out.println(b1);
        Except();

        Snake s1 = new Snake("Java");
        s1.eat();
        s1.crawl();
        s1.walk();
        s1.speak();
        s1.sleep();
        s1.toString();
        System.out.println(s1);
        Except();

        Fish f1 = new Fish("Ruwi");
        f1.eat();
        f1.swim();
        f1.walk();
        f1.speak();
        f1.sleep();
        f1.toString();
        System.out.println(f1);
        Except();

        Crab cr1 = new Crab("Photea");
        cr1.eat();
        cr1.swim();
        cr1.walk();
        cr1.speak();
        cr1.sleep();
        cr1.toString();
        System.out.println(cr1);
        Except();

        Bird bi = new Bird("Mikki");
        bi.eat();
        bi.fly();
        bi.walk();
        bi.speak();
        bi.sleep();
        bi.toString();
        System.out.println(bi);
        Except();

        System.out.println("Polymorphism Test is ------------");
        Animal[] animals = {h1, c1, d1, co1, b1, s1, f1, cr1, bi};
        for (int i = 0; i < animals.length; i++) {
            if (animals[i] instanceof Animal) {
                animals[i].eat();
                animals[i].walk();
                animals[i].speak();
                animals[i].sleep();

            }
            if (animals[i] instanceof LandAnimal) {
                System.out.println("Im in LandAnimal Class ");
                ((LandAnimal) (animals[i])).run();

            }
            if (animals[i] instanceof Reptile) {
                System.out.println("Im in Reptile Class ");
                ((Reptile) (animals[i])).crawl();

            }
            if (animals[i] instanceof Poultry) {
                System.out.println("Im in Poultry Class ");
                ((Poultry) (animals[i])).fly();

            }
            if (animals[i] instanceof AquaticAnimal) {
                System.out.println("Im in Aquatic Class ");
                ((AquaticAnimal) (animals[i])).swim();

            }
            Except();

        }

        System.out.println("Polymorphism Test 2 is ------------");
        for (int i = 0; i < animals.length; i++) {
            System.out.println(animals[i].getName() + " Instanceof Animal " + (animals[i] instanceof Animal));
            System.out.println(animals[i].getName() + " Instanceof Aquatic " + (animals[i] instanceof AquaticAnimal));
            System.out.println(animals[i].getName() + " Instanceof LandAnimal " + (animals[i] instanceof LandAnimal));
            System.out.println(animals[i].getName() + " Instanceof Poultry " + (animals[i] instanceof Poultry));
            System.out.println(animals[i].getName() + " Instanceof Reptile " + (animals[i] instanceof Reptile));
            System.out.println(animals[i].getName() + " Instanceof Object " + (animals[i] instanceof Object));
            Except();
        }
    }

    private static void Except() {
        System.out.println("");

    }
}
